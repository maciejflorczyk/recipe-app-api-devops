terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.54.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-central-1"
}
