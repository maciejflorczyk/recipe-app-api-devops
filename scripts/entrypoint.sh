#!/bin/sh

set -e #exit the scripts when errors

python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate

# run uwsgi server
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
