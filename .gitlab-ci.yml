image:
  name: hashicorp/terraform:0.12.21
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

stages:
  - Test and Lint
  - Build and Push
  - Staging Plan
  - Staging Apply
  - Production Plan
  - Production Apply
  - Destroy #manual jobs to destroy the environment

Test and Lint:
  image: docker:19.03.5
  services:
    - docker:19.03.5-dind
  stage: Test and Lint
  script:
#    - apk add python3-dev libffi-dev openssl-dev gcc libc-dev make
#    - pip3 install docker-compose
    - apk add --update docker-compose
    - docker-compose run --rm app sh -c "python manage.py wait_for_db && python manage.py test && flake8"
  rules:
    # target branch for merge request - we want it to run test before merge
    # we have a merge request, and it's going to be merged into master or production
    # or if it's master or production
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(master|production)$/ || $CI_COMMIT_BRANCH =~ /^(master|production)$/'

Validate Terraform:
  stage: Test and Lint
  script:
    - cd deploy/
    #initialize by cloning the terraform providers etc. Backend false because we don't provide credentials until we are on the protected branch (this is how we setup Gitlab CI/CD). Here we might run it, while not in protected branch. We also don't need it for validation.
    - terraform init -backend=false
    - terraform validate
    #we don't want to change the code, we just want to check the code, if it should be reformated. The job will fail if the developer has not done the "fmt" himself.
    - terraform fmt -check
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(master|production)$/ || $CI_COMMIT_BRANCH =~ /^(master|production)$/'

Build and Push:
  image: docker:19.03.5
  services:
    - docker:19.03.5-dind
  stage: Build and Push
  script:
    - apk add python3
    - pip3 install awscli
    - docker build --compress -t $ECR_REPO:$CI_COMMIT_SHORT_SHA .
    - $(aws ecr get-login --no-include-email --region eu-central-1)
    - docker push $ECR_REPO:$CI_COMMIT_SHORT_SHA
    # tag again as a latest
    - docker tag $ECR_REPO:$CI_COMMIT_SHORT_SHA $ECR_REPO:latest
    - docker push $ECR_REPO:latest
  rules:
    # whenever we merge into master or production
    - if: '$CI_COMMIT_BRANCH =~ /^(master|production)$/'

Staging Plan:
  stage: Staging Plan
  script:
    - cd deploy/
    - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
    - terraform init
    - terraform workspace select staging || terraform workspace new staging
    - terraform plan
  rules:
    # production needs to be tested also in staging - this is a good practice
    - if: '$CI_COMMIT_BRANCH =~ /^(master|production)$/'

Staging Apply:
  stage: Staging Apply
  script:
    - cd deploy/
    - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
    - terraform init
    - terraform workspace select staging
    - terraform apply -auto-approve
  rules:
    # production needs to be tested also in staging - this is a good practice
    - if: '$CI_COMMIT_BRANCH =~ /^(master|production)$/'

Production Plan:
  stage: Production Plan
  script:
    - cd deploy/
    - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
    - terraform init
    - terraform workspace select production || terraform workspace new production
    - terraform plan
  rules:
    - if: '$CI_COMMIT_BRANCH == "production"'

Production Apply:
  stage: Production Apply
  script:
    - cd deploy/
    - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
    - terraform init
    - terraform workspace select production
    - terraform apply -auto-approve
  rules:
    # production needs to be tested also in Production - this is a good practice
    - if: '$CI_COMMIT_BRANCH == "production"'

Staging Destroy:
  stage: Destroy
  script:
    - cd deploy/
    - terraform init
    - terraform workspace select staging
    - terraform destroy -auto-approve
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(master|production)$/'
      when: manual

Production Destroy:
  stage: Destroy
  script:
    - cd deploy/
    - terraform init
    - terraform workspace select production
    - terraform destroy -auto-approve
  rules:
    - if: '$CI_COMMIT_BRANCH == "production"'
      when: manual
